<?php
use Restserver\Libraries\REST_Controller;

require(APPPATH . 'libraries/REST_Controller.php'); // Agregado por MPRIETO para que funcione

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//require APPPATH . 'libraries/REST_Controller.php';

/**
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {

  function __construct() {
    parent::__construct();

    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    // MPRIETO: Se pueden utilizar limit=integer - log=booleano - level=integer - key=booleano
    $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
    $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['users_put']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

    // MPRIETO: level
    // 0 = Full Acceso
    // 1 = Solo Consultar
    // 2 = Consultar y Editar
    $this->methods['users_get']['level'] = 0; // Agregando permisos

    $this->load->model('UsersApi_model', 'GetModel');

  }

  // GET DATA ONE OR ALL
  public function users_get() {

    $id = (int) $this->get('id');

    if ($id === NULL || empty($id) || !isset($id)) {

      $result = $this->GetModel->get_all();

      if ($result) {
        $this->response($result, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }else {
        $this->response([
          'status' => FALSE,
          'message' => 'The user was not found'
          ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }
    }

    if ($id <= 0) {
      $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $result = $this->GetModel->get($id);

    if ($result) {
      $this->response($result, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    }else {
      $this->response([
        'status' => FALSE,
        'message' => 'The user was not found'
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
  }


  // CREATE
  public function users_post() {

    $inputs = $this->post();

    if (empty($inputs['username']) || $inputs['username'] == NULL || empty($inputs['password']) || $inputs['password'] == NULL) {
      $this->response([
        'status' => FALSE,
        'message' => 'The required data was not received'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $values = array(
      'username' => $inputs['username'], 
      'password' => $inputs['password'],
      'active' => $inputs['active'],
      'email' => $inputs['email'],
    );

    $result = $this->GetModel->create($values);

    if ($result) {
      $this->set_response([
        'status' => TRUE,
        'message' => 'Record created'
        ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }else {
      $this->response([
        'status' => FALSE,
        'message' => 'Error when creating record'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }
  }


  /*
   * UPDATE
   * NOTA: Enviar formulario con formato de body: x-www-form-urlencoded
   */
  public function users_put() {

    $id = (int) $this->get('id');

    if ($id <= 0) {
      $this->response([
        'status' => FALSE,
        'message' => 'Id Incorrect'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }
   
    $inputs = $this->put();

    if (empty($inputs['username']) || $inputs['username'] == NULL || empty($inputs['password']) || $inputs['password'] == NULL) {
      $this->response([
        'status' => FALSE,
        'message' => 'The required data was not received'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $values = array(
      'username' => $inputs['username'], 
      'password' => $inputs['password'],
      'active' => $inputs['active'],
      'email' => $inputs['email'],
    );
    
    $result = $this->GetModel->update($id, $values);

    if ($result) {
      $this->set_response([
        'status' => TRUE,
        'message' => 'Record update'
        ], REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }else {
      $this->response([
        'status' => FALSE,
        'message' => 'Error when update record'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }
  }

  /*
   * DELETE
   */
  public function users_delete() {
    $id = (int) $this->get('id');

    if ($id <= 0) {
      $this->response([
        'status' => FALSE,
        'message' => 'The required data was not received'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $result = $this->GetModel->delete('id', $id);

    if ($result) {
      $this->set_response([
        'status' => TRUE,
        'message' => 'Record Deleted'
        ], REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
    }else {
      $this->response([
        'status' => FALSE,
        'message' => 'Error when delete record'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

  }

}
