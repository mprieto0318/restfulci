<?php
use Restserver\Libraries\REST_Controller;

require(APPPATH . 'libraries/REST_Controller.php'); // Agregado por MPRIETO para que funcione

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//require APPPATH . 'libraries/REST_Controller.php';

/**
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class LoginRegister extends REST_Controller {

  function __construct() {
    parent::__construct();

    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    // MPRIETO: Se pueden utilizar limit=integer - log=booleano - level=integer - key=booleano
    //$this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
    //$this->methods['login_post']['limit'] = 100; // 100 requests per hour per user/key
    //$this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

    // MPRIETO: level
    // 0 = Full Acceso
    // 1 = Solo Consultar
    // 2 = Consultar y Editar
    //$this->methods['users_get']['level'] = 0; // Agregando permisos

    $this->load->model('LoginRegister_model', 'GetModel');

  }

  public function login_post() {

    $inputs = $this->post();

    if (empty($inputs['username']) || $inputs['username'] == NULL || empty($inputs['password']) || $inputs['password'] == NULL) {
      $this->response([
        'status' => FALSE,
        'message' => 'The required data was not received'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $valuesEmail = array(
      'email' => $inputs['username'], 
      'password' => sha1($inputs['password']), 
      'active' => 1, 
    );

    $valuesUsername = array(
      'username' => $inputs['username'], 
      'password' => sha1($inputs['password']), 
      'active' => 1, 
    );

    //$result = $this->User_model->login(clean_data($valuesEmail), clean_data($valuesUsername));
    $result = $this->GetModel->login($valuesEmail, $valuesUsername);

    if ($result) {
      // Actualizar ultimo inicio de session
      $this->GetModel->update_last_login($result->id);
      
      $this->response($result, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    }else {
      $this->response([
        'status' => FALSE,
        'message' => 'Bad credentials or the user does not exist'
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
  }

  /*
  public function users_get_OLD() {
    // Users from a data store e.g. database
    $users = [
      ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
      ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
      ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
    ];

    $id = $this->get('id');

    // If the id parameter doesn't exist return all the users

    if ($id === NULL) {
      // Check if the users data store contains users (in case the database result returns NULL)
      if ($users) {
        // Set the response and exit
        $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
      }else{
        // Set the response and exit
        $this->response([
          'status' => FALSE,
          'message' => 'No users were found'
        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
      }
    }

    // Find and return a single record for a particular user.
    $id = (int) $id;

    // Validate the id.
    if ($id <= 0){
      // Invalid id, set the response and exit.
      $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    // Get the user from the array, using the id as key for retrieval.
    // Usually a model is to be used for this.
    $user = NULL;

    if (!empty($users)) {
      foreach ($users as $key => $value)
      {
        if (isset($value['id']) && $value['id'] === $id)
        {
          $user = $value;
        }
      }
    }

    if (!empty($user))
    {
      $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
    }
    else
    {
      $this->set_response([
        'status' => FALSE,
        'message' => 'User could not be found'
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
  }
  */


  public function users_post() {
    $inputs = $this->post();

    if (empty($inputs['username']) || $inputs['username'] == NULL || empty($inputs['password']) || $inputs['password'] == NULL) {
      $this->response([
        'status' => FALSE,
        'message' => 'The required data was not received'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $values = array(
      'username' => $inputs['username'], 
      'password' => $inputs['password'],
    );

    $result = $this->GetModel->create($values);

    if ($result) {
      $this->set_response([
        'status' => TRUE,
        'message' => 'Record created'
        ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }else {
      //$this->set_response([
      //  'status' => FALSE,
      //  'message' => 'Error when creating record'
      //  ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code

      $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }
  }

  /*
  public function users_pos_OLDt() {
    // $this->some_model->update_user( ... );
    $message = [
        'id' => 100, // Automatically generated by the model
        'name' => $this->post('name'),
        'email' => $this->post('email'),
        'message' => 'Added a resource'
      ];

    $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
  }
  */

  function update_post() {
   
    $inputs = $this->post();

    if (empty($inputs['username']) || $inputs['username'] == NULL || empty($inputs['password']) || $inputs['password'] == NULL  || empty($inputs['id']) || $inputs['id'] == NULL) {
      $this->response([
        'status' => FALSE,
        'message' => 'The required data was not received'
        ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
    }

    $values = array(
      'username' => $inputs['username'], 
      'password' => $inputs['password'],
    );
    
    $result = $this->GetModel->update($inputs['id'], $values);

    //$msg = '<br><p class="text-danger">No se pudo actualizar el usuario intente mas tarde</p>';
    if ($result) {
      $msg = '<br><p class="text-success">' . _('Updated') . ' <i class="fa fa-check" aria-hidden="true"></i></p>';
    }else {
      $msg = '<br><p class="text-danger">' . _('Edit the content to update') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

      public function users_delete()
      {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
          }

        // $this->some_model->delete_something($id);
          $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
          ];

        //$this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
          $this->set_response($message, REST_Controller::HTTP_OK); 


        }

      }
