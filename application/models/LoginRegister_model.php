<?php

class LoginRegister_model extends CI_Model {
  public $tbl;

  public function __construct() {
    parent::__construct();
    $this->tbl = 'users_api';
  }

  function login($valuesEmail, $valuesUsername) {

    return $this->db->from($this->tbl)
    ->group_start()
    ->where($valuesEmail)
    ->or_group_start()
    ->where($valuesUsername)
    ->group_end()
    ->group_end()
    ->get()->row();

    //$this->db->select('id, username, email')->where($values);
    //return $this->db->get($this->tbl)->row();
  }

  function update_last_login($id) {
    $this->db->update($this->tbl, array('last_login' => time()), array('id' => $id));

    return $this->db->affected_rows() == 1;
  }


  //function user_exists($values) {
    //$this->db->from($this->tbl)->where($values);

    //return ($this->db->get()->num_rows() > 0) ? TRUE : FALSE;
  //}


}