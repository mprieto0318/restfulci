<?php

class UsersApi_model extends CI_Model
{
  public $tbl;

  public function __construct() {
    parent::__construct();
    $this->tbl = 'users_api';
  }

  public function get($id){
    $this->db->select("username,register_date");
    $query = $this->db->get_where($this->tbl, array("id" => $id));
    if($query->num_rows() == 1){
      return $query->row();
    }
  }

  public function get_all(){
    $query = $this->db->get($this->tbl);
    if($query->num_rows() > 0){
      return $query->result();
    }
  }

  function create($values) {
    return $this->db->insert($this->tbl, $values);
  }

  function update($id, $values) {
    $this->db->where('id', $id);
    $this->db->update($this->tbl, $values);

    return $this->db->affected_rows();
  }

  function delete($column, $value) {
    return $this->db->delete($this->tbl, array($column => $value));
  }

  public function posts($id){
    $this->db->where("user_id", $id);
    $query = $this->db->get("messages_api");
    if($query->num_rows() > 0){
      return $query->result();
    }
  }

  



}