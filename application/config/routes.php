<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8

$route['api/prueba/users/(:num)'] = 'api/Prueba/users/id/$1'; // Example 4
$route['api/prueba/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/Prueba/users/id/$1/format/$3$4'; // Example 8



// register
$route['register'] = 'Register/index';
$route['register/create'] = 'Register/newApiKey';

// Login and Register
$route["api/login"]["post"] = "api/LoginRegister/login"; //insert

// Obtener Usuarios Api
$route["api/users/(:num)"]["get"] = "api/Users/users/id/$1"; // Get Id
$route["api/users"]["get"] = "api/Users/users"; // Get All
$route['api/users/(:num)/format/(:any)']["get"] = 'api/Users/users/id/$1/format/$2'; // Get Formato Id
$route['api/users/format/(:any)']["get"] = 'api/Users/users/format/$1'; // Get Formato All

$route["api/users"]["post"] = "api/Users/users"; // Insert

$route["api/users/(:num)"]["put"] = "api/Users/users/id/$1"; // Update

$route["api/users/(:num)"]["delete"] = "api/Users/users/id/$1"; // Delete


//$route["api/users/(:num)"]["delete"] = "Apicontroller/index_delete/$1"; //delete
//$route['api/users/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/Users/users/id/$1/format/$3$4'; // Para enviar Formato



